#include "rssfeedmodel.h"

#include <QNetworkReply>
#include <QtXmlPatterns/QXmlQuery>

RssFeedModel::RssFeedModel(const QString &feedUrl, QObject *parent) : QAbstractListModel(parent)
{
    auto *reply = _nam.get(QNetworkRequest{QUrl{feedUrl}});

    connect(reply, &QNetworkReply::finished, this, [=](){
        QString feedXml = QString::fromLatin1(reply->readAll());

        QXmlQuery query;
        query.setFocus(feedXml);
        query.setQuery(QStringLiteral("//rss/channel/item/title/string()"));
        query.evaluateTo(&_postTitles);

        Q_EMIT dataChanged(index(0), index(_postTitles.count()-1));

        //delete reply //da problema pois se houver algo executado depois do emit do signal finished e o reply tiver sido deletado, vai da erro
        reply->deleteLater(); //deleta o reply assim que finalizar o eventloop
    });
}

auto RssFeedModel::rowCount(const QModelIndex &parent) const -> int
{
    Q_UNUSED(parent)
    return _postTitles.count();
}

auto RssFeedModel::data(const QModelIndex &index, int role) const -> QVariant
{
    if (!index.isValid() || index.row() >= _postTitles.count()) {
        return {};
    }

    if (role == Qt::DisplayRole) {
        return _postTitles.at(index.row());
    }

    return {};
}

auto RssFeedModel::headerData(int section, Qt::Orientation orientation, int role) const -> QVariant
{
    if (role != Qt::DisplayRole) {
        return {};
    }
    if (orientation == Qt::Horizontal) {
        return QStringLiteral("Title");
    }
    return QString::number(section);
}
